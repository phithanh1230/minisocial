import { ADD_EMOTION,DELETE_EMOTION,FETCH_EMOTION,UPDATE_EMOTION } from '../_consts/emotion.const';
import axios from 'axios';

const API_LOG_URL="https://localhost:44399/api/emotions";

export const CreateEmotionSuccess=(data)=>{
    return {
        type:ADD_EMOTION,
        payload:data
    }
}

export const CreateEmotion=(data)=>{
    return (dispatch) => {
        return axios.post(API_LOG_URL, data)
                .then(res=>{
                    dispatch(CreateEmotionSuccess(res.data));
                })
                .catch(err=>{
                    throw(err);
                })
    }
}


export const FetchAllEmotionsSuccess=(data)=>{
    return {
        type:FETCH_EMOTION,
        data
    }
}

export const FetchAllEmotions=()=>{
    return (dispatch) =>{
        return axios.get(API_LOG_URL)
                .then(res=>{
                    dispatch(FetchAllEmotionsSuccess(res.data))
                })
                .catch(err=>{
                    throw(err);
                })
    }
}
