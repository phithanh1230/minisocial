import { ADD_EMOTION,DELETE_EMOTION,FETCH_EMOTION,UPDATE_EMOTION } from '../_consts/emotion.const';

const emotions = (state=[],action)=>{
    switch(action.type){
        case ADD_EMOTION:
            return [action.payload,...state];
        case DELETE_EMOTION:
            return action.Id;
        case FETCH_EMOTION:
            return action.emotions;
        case UPDATE_EMOTION:
            return [...state];
    }
}

export default emotions;