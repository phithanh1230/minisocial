import emotions from '../_reducers/emotions.reducer';
import { combineReducers } from 'redux'

const myReducers= combineReducers({
    emotions
});
export default myReducers;
