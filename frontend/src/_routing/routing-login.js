import { BrowserRouter, Route,Switch,Redirect } from "react-router-dom";
import React, { Component } from 'react';

// import Login from "./login/login";
import App from "../App";

class RouterLogin extends Component{
    render(){
        return(
            <BrowserRouter>  
                <Switch>
                    {/* <Route exact path="/login" component={Login} /> */}
                    <Route path="/" component={App} />
                    {/* <Redirect from="**" to="/login"/> */}
                </Switch>
            </BrowserRouter>
        );
    }
}
export default RouterLogin;