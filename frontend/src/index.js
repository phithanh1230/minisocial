import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

//Redux
import thunk from 'redux-thunk';
import { createStore,applyMiddleware  } from 'redux';
import myReducers from './_reducers/myReducers';
import { Provider } from 'react-redux'
import RouterLogin from './_routing/routing-login';
import { FetchAllEmotions } from './_actions/emotion.action';

const store = createStore(myReducers,applyMiddleware(thunk))
store.dispatch(FetchAllEmotions());

ReactDOM.render(
                <Provider store={store} >
                        <RouterLogin />
                </Provider>, 
                document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
