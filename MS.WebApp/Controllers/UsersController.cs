﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MS.Data.Helpers;
using MS.Data.Models;
using MS.Services;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace MS.WebApp.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        #region Fields
        private readonly IUserServices _userServices;
        #endregion

        #region Ctor
        public UsersController(IUserServices userservices)
        {
            _userServices = userservices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<User> Get()
        {
            return  _userServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public User Get(Guid id)
        {
            return _userServices.FindById(id);
        }
        #endregion

        #region POST
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void Post(User entity)
        {
            _userServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, User entity)
        {
            _userServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _userServices.Delete(id);
        }
        #endregion

    }
}