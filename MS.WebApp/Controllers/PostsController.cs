﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;
using System;
using System.Linq;

namespace MS.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        #region Fields
        private readonly IPostServices _postServices;
        #endregion

        #region Ctor
        public PostsController(IPostServices postServices)
        {
            _postServices = postServices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<Post> Get()
        {
            return _postServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Post Get(Guid id)
        {
            return _postServices.FindById(id);
        }
        #endregion

        #region POST
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void Post(Post entity)
        {
            _postServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, Post entity)
        {
            _postServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _postServices.Delete(id);
        }
        #endregion
    }
}