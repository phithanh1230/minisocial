﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;
using System;
using System.Linq;

namespace MS.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        #region Fields
        private readonly IMessageServices _messageServices;
        #endregion

        #region Ctor
        public MessagesController(IMessageServices messageServices)
        {
            _messageServices = messageServices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<Message> Get()
        {
            return _messageServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Message Get(Guid id)
        {
            return _messageServices.FindById(id);
        }
        #endregion

        #region POST
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void Post(Message entity)
        {
            _messageServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, Message entity)
        {
            _messageServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _messageServices.Delete(id);
        }
        #endregion
    }
}