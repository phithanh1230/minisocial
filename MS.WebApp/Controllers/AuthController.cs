﻿using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;

namespace MS.WebApp.Controllers
{
    public class AuthController : ControllerBase
    {
        #region Fields
        private readonly IAuthServices _authServices;
        #endregion

        #region Ctor
        public AuthController(IAuthServices authServices)
        {
            _authServices = authServices;
        }
        #endregion

        #region ChangePassword
        [HttpPost]
        [Route("ChangePassword")]
        public IActionResult ChangePassword([FromBody] TokenRequest tokenRequest)
        {
            var result = _authServices.ChangePassword(tokenRequest);
            if (result == false)
            {
                return null;
            }
            return Ok();
        }
        #endregion

        #region Register
        [HttpPost]
        [Route("Register")]
        public IActionResult Register([FromBody] TokenRequest tokenRequest)
        {
            var result = _authServices.Register(tokenRequest);
            if (result == false) {
                return null;
            }
            return Ok();
        }
        #endregion

        #region Authentication
        [HttpPost]
        [Route("Login")]
        public IActionResult Authenticate([FromBody] TokenRequest tokenRequest)
        {
            var jwtToken = _authServices.Login(tokenRequest);
            if (jwtToken == null)
            {
                return Unauthorized();
            }
            return Ok(jwtToken);
        }
        #endregion
    }
}
