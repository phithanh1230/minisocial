﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;
using System;
using System.Linq;

namespace MS.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TextMessagesController : ControllerBase
    {
        #region Fields
        private readonly ITextMessageServices _textMessageServices;
        #endregion

        #region Ctor
        public TextMessagesController(ITextMessageServices TextMessageservices)
        {
            _textMessageServices = TextMessageservices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<TextMessage> Get()
        {
            return _textMessageServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public TextMessage Get(Guid id)
        {
            return _textMessageServices.FindById(id);
        }
        #endregion

        #region TextMessage
        /// <summary>
        /// TextMessage
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void TextMessage(TextMessage entity)
        {
            _textMessageServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, TextMessage entity)
        {
            _textMessageServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _textMessageServices.Delete(id);
        }
        #endregion
    }
}