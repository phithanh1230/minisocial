﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;
using System;
using System.Linq;

namespace MS.WebApp.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmotionsController : ControllerBase
    {
        #region Fields
        private readonly IEmotionServices _emotionServices;
        #endregion

        #region Ctor
        public EmotionsController(IEmotionServices emotionServices)
        {
            _emotionServices = emotionServices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<Emotion> Get()
        {
            return _emotionServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Emotion Get(Guid id)
        {
            return _emotionServices.FindById(id);
        }
        #endregion

        #region POST
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void Post(Emotion entity)
        {
            _emotionServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, Emotion entity)
        {
            _emotionServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _emotionServices.Delete(id);
        }
        #endregion
    }
}