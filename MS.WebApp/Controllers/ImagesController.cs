﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MS.Data.Models;
using MS.Services;
using System;
using System.Linq;

namespace MS.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        #region Fields
        private readonly IImageServices _imageServices;
        #endregion

        #region Ctor
        public ImagesController(IImageServices imageServices)
        {
            _imageServices = imageServices;
        }
        #endregion

        #region GET
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<Image> Get()
        {
            return _imageServices.FindAll();
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Image Get(Guid id)
        {
            return _imageServices.FindById(id);
        }
        #endregion

        #region POST
        /// <summary>
        /// POST
        /// </summary>
        /// <param name="entity"></param>
        [HttpPost]
        public void Post(Image entity)
        {
            _imageServices.Add(entity);
        }
        #endregion

        #region PUT
        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        [HttpPut("{id}")]
        public void Put(Guid id, Image entity)
        {
            _imageServices.Update(id, entity);
        }
        #endregion

        #region DELETE
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _imageServices.Delete(id);
        }
        #endregion
    }
}