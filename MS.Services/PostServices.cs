﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface IPostServices : IBaseServices<Post>
    {

    }

    public class PostServices : BaseServices<Post>, IPostServices
    {
        public PostServices(IRepository<Post> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
