﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface IEmotionServices : IBaseServices<Emotion>
    {

    }

    public class EmotionServices : BaseServices<Emotion>, IEmotionServices
    {
        public EmotionServices(IRepository<Emotion> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
