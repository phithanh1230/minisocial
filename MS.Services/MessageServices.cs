﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface IMessageServices : IBaseServices<Message>
    {

    }

    public class MessageServices : BaseServices<Message>, IMessageServices
    {
        public MessageServices(IRepository<Message> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
