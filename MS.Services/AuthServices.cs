﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MS.Data.Helpers;
using MS.Data.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace MS.Services
{
    public interface IAuthServices
    {
        string Login(TokenRequest tokenRequest);
        bool Register(TokenRequest tokenRequest);
        bool ChangePassword(TokenRequest tokenRequest);

    }

    public class AuthServices : IAuthServices
    {
        private readonly ITokenRequestServices _tokenRequestServices;
        private readonly AppSettings _appSettings;

        public AuthServices(ITokenRequestServices tokenRequestServices, IOptions<AppSettings> appSettings)
        {
            _tokenRequestServices = tokenRequestServices;
            _appSettings = appSettings.Value;
        }

        public bool ChangePassword(TokenRequest tokenRequest) {
            var user = _tokenRequestServices.FindAll().Where(u => u.Username == tokenRequest.Username).SingleOrDefault();
            if (user == null) {
                return false;
            }
            user.Password = tokenRequest.Password;
            _tokenRequestServices.Update(user);
            return true;
        }

        public bool Register(TokenRequest tokenRequest) {
            var user = _tokenRequestServices.FindAll().Where(u => u.Username == tokenRequest.Username).SingleOrDefault();
            if (user == null) {
                _tokenRequestServices.Add(tokenRequest);
                return true;
            }
            return false;
        }

        public string Login(TokenRequest tokenRequest)
        {
            var user = _tokenRequestServices.FindAll().Where(u => u.Username == tokenRequest.Username && u.Password == tokenRequest.Password).SingleOrDefault();

            if (user == null)
            {
                return null;
            }
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.Name,user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            return user.Token;
        }
    }
}
