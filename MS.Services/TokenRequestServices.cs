﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface ITokenRequestServices : IBaseServices<TokenRequest>
    {

    }
    public class TokenRequestServices : BaseServices<TokenRequest>, ITokenRequestServices
    {
        public TokenRequestServices(IRepository<TokenRequest> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
