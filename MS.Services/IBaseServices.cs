﻿using MS.Data.Infrastructure;
using System;
using System.Linq;

namespace MS.Services
{
    public interface IBaseServices<T> where T : BaseEntity
    {
        IQueryable<T> FindAll();
        T FindById(Guid id);
        void Add(T entity);
        void Update(T entity);
        void Update(Guid id, T entity);
        void Delete(T entity);
        void Delete(Guid id);
    }

    public abstract class BaseServices<T> : IBaseServices<T> where T : BaseEntity
    {
        private readonly IRepository<T> _repo;
        private readonly IUnitOfWork _unitOfWork;

        public BaseServices(IRepository<T> repo, IUnitOfWork unitOfWork) {
            _repo = repo;
            _unitOfWork = unitOfWork;
        }

        public void Add(T entity)
        {
            _repo.Add(entity);
            _unitOfWork.Commit();
        }

        public void Delete(T entity)
        {
            _repo.Delete(entity);
        }

        public void Delete(Guid id)
        {
            var entity = FindById(id);
            if (entity == null)
            {
                return;
            }
            _repo.Delete(entity);
        }

        public IQueryable<T> FindAll()
        {
            return _repo.Find(w => w.IsDeleted == false);
        }

        public T FindById(Guid id)
        {
            return _repo.Find(w => w.IsDeleted == false && w.Id == id).SingleOrDefault();
        }

        public void Update(T entity)
        {
            Update(entity.Id, entity);
        }

        public void Update(Guid id, T entity)
        {
            _repo.Update(id, entity);
            _unitOfWork.Commit();
        }
    }

}
