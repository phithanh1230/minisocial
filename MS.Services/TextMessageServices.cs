﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface ITextMessageServices : IBaseServices<TextMessage>
    {

    }

    public class TextMessageServices : BaseServices<TextMessage>, ITextMessageServices
    {
        public TextMessageServices(IRepository<TextMessage> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
