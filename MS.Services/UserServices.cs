﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface IUserServices : IBaseServices<User>
    {
    }

    public class UserServices : BaseServices<User>, IUserServices
    {
        public UserServices(IRepository<User> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
