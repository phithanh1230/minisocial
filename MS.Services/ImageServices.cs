﻿using MS.Data.Infrastructure;
using MS.Data.Models;

namespace MS.Services
{
    public interface IImageServices : IBaseServices<Image>
    {

    }

    public class ImageServices : BaseServices<Image>, IImageServices
    {
        public ImageServices(IRepository<Image> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
