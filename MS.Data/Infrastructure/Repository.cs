﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MS.Data.Infrastructure
{
    public class Repository<T> : IRepository<T> where T:BaseEntity
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<T> _dbset;

        //public Repository(ApplicationDbContext context) {
        //    _context = context;
        //}
        public Repository() {
            _context = new ApplicationDbContext();
            _dbset = _context.Set<T>();
        }

        public void Add(T entity)
        {
            _dbset.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            entity.IsDeleted = true;
            Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Expression<Func<T, bool>> where)
        {
            var entities = _context.Set<T>().Where(where).AsEnumerable();
            foreach (var item in entities)
            {
                Delete(item);
            }
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = Find(w => w.IsDeleted == false && w.Id == id).SingleOrDefault();
            entity.IsDeleted = true;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where);
        }

        public void Update(T entity)
        {
            Update(entity.Id, entity);
        }

        public void Update(Guid id, T entity)
        {
            var item = Find(w => w.IsDeleted == false && w.Id == id).SingleOrDefault();
            if (item == null)
            {
                return;
            }
            entity.Id = id;
            entity.CreateDate = item.CreateDate;
            _context.Entry(item).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }
    }
}
