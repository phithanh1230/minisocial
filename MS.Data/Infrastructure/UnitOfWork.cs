﻿namespace MS.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseFactory _databaseFactory;
        private readonly ApplicationDbContext _context;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
            _context = _databaseFactory.Get();
        }
        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
