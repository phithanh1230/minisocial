﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MS.Data.Infrastructure
{
    public class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifyDate { get; set; }
        public bool Status { get; set; }
        public bool IsDeleted { get; set; }

        public BaseEntity() {
            CreateDate = DateTime.Now;
            LastModifyDate = DateTime.Now;
            Status = true;
            IsDeleted = false;
        }
    }
}
