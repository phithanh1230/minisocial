﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace MS.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Update(Guid id, T entity);
        void Delete(Guid id);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        IQueryable<T> Find(Expression<Func<T, bool>> where);
    }
}
