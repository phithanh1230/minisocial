﻿namespace MS.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
