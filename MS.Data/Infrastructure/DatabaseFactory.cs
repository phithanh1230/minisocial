﻿namespace MS.Data.Infrastructure
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private ApplicationDbContext _context;
        public void Dispose()
        {
            _context.Dispose();
        }

        public ApplicationDbContext Get()
        {
            return _context ?? (_context = new ApplicationDbContext());
        }
    }
}
