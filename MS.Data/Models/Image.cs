﻿using MS.Data.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace MS.Data.Models
{
    public class Image : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        [Display(Name = "Image's Url")]
        public string ImageUrl { get; set; }
        [Display(Name = "Image's Data")]
        public string ImageBase64 { get; set; }
    }
}
