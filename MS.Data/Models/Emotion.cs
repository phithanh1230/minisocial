﻿using MS.Data.Infrastructure;
using System;

namespace MS.Data.Models
{
    public class Emotion : BaseEntity
    {
        public Guid ImageId { get; set; }
        public Image Image { get; set; }
    }
}
