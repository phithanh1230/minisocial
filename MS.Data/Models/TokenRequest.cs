﻿using MS.Data.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace MS.Data.Models
{
    public class TokenRequest:BaseEntity
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
