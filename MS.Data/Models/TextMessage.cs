﻿using MS.Data.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;

namespace MS.Data.Models
{
    public class TextMessage : BaseEntity
    {
        [Required]
        public string Text { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
