﻿using MS.Data.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MS.Data.Models
{
    public class Post : BaseEntity
    {
        [Required]
        public string Content { get; set; }

        [Display(Name = "Is Comment")]
        public bool IsComment { get; set; }
        
        public Guid UserId { get; set; }
        public User User { get; set; }

        public IQueryable<Post> Comments { get; set; }
        public IQueryable<Emotion> Emotions { get; set; }

        public Post() {
            IsComment = false;
        }
    }
}
