﻿using MS.Data.Infrastructure;
using System.Linq;

namespace MS.Data.Models
{
    public class Message : BaseEntity
    {
        public IQueryable<TextMessage> Texts { get; set; }

        public User User { get; set; }

        public User Friend { get; set; }
    }
}
