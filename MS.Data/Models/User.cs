﻿using MS.Data.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MS.Data.Models
{
    public class User : BaseEntity
    {
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^?([0-9]{10})",
                   ErrorMessage = "Entered phone format is not valid.")]
        public string Phone { get; set; }

        public Guid ImageId { get; set; }
        public Image Avatar { get; set; }

        public IQueryable<User> Friends { get; set; }
        public IQueryable<Post> Posts { get; set; }
    }
}
