﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MS.Data.Models;
using System.IO;

namespace MS.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Emotion> Emotions { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<TextMessage> TextMessages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TokenRequest> TokenRequest { get; set; }

        public ApplicationDbContext() { }
        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("DbConnectionString");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
    }
}
